<!DOCTYPE>
<html>
	<head> 
	  <title> Admin - Kentucky Association of Food Banks </title>
		<link type = "text/css" rel = "stylesheet" href = "css/main.css"/>
		<script type="text/javascript" src="javascript/jquery.js"></script>
	</head>
<?php	
	ob_start();	
	echo "<div id  = 'admin_menu'>";
	echo "<div id  = 'move_panel'>";
	echo "<a href ='displayallcontacts.php'> Show Me All Contacts </a>";
	echo " | ";
	echo "<a href ='track.php'> Track Communication </a>";
	echo " | ";
	echo "<a href ='logout.php'> Logout </a>";
	if(isset($_SESSION['username'])){
    	echo"Welcome";echo "\x20\x20\x20"; echo $_SESSION['username'];
	}
	echo "</div>";
	echo "</div>";
	?>
		<!--- Add Data to Database --->
		<div class = "left_col">
			<div id = "left_nav">
				  <h1> Add New Data </h1>
				  <div id = "Inner_Panel">
					<ul>
						<li><a href = "addcategory.php"><img src = "images/arrow_button2.jpg"/> New Category </a></li>
						<li><a href = "addcommtype.php"><img src = "images/arrow_button2.jpg"/> New Comm Type </a></li>
						<li><a href = "addcontact.php"><img src = "images/arrow_button2.jpg"/>  New Contact </a></li>
						<li><a href = "adddonation.php"><img src = "images/arrow_button2.jpg"/> New Donation </a></li>
						<li><a href = "addnewfunders.php"><img src = "images/arrow_button2.jpg"/> New Funders </a></li>
						<li><a href = "addnewfoodbank.php"><img src = "images/arrow_button2.jpg"/> New FoodBank</a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/>  New Program </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> New Partner </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> Crop Quantity </a></li>
					</ul>
				  </div>
			</div>
			<div id = "left_nav">
				 <h1> Update Data </h1>
				 <div id = "Inner_Panel">
				 <ul>
						<li><a href = "displaycategories.php"><img src = "images/arrow_button2.jpg"/> Update Category </a></li>
						<li><a href = "displayallcontacts.php"><img src = "images/arrow_button2.jpg"/>  Update Contact </a></li>
						<li><a href = "adddonation.php"><img src = "images/arrow_button2.jpg"/> Update Donation </a></li>
						<li><a href = "addnewfunders.php"><img src = "images/arrow_button2.jpg"/> Update Funders </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/>  Update Program </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> Update Partner </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> Update Quantity </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> Update FoodBank</a></li>
					</ul>
				  </div>
			</div>
			<div id = "left_nav">
				 <h1> Delete Data</h1>
				 <div id = "Inner_Panel">
				 <ul>
						<li><a href = "addcategory.php"><img src = "images/arrow_button2.jpg"/> Delete Category </a></li>
						<li><a href = "addcontact.php"><img src = "images/arrow_button2.jpg"/>  Delete Contact </a></li>
						<li><a href = "adddonation.php"><img src = "images/arrow_button2.jpg"/> Delete Donation </a></li>
						<li><a href = "addnewfunders.php"><img src = "images/arrow_button2.jpg"/> Delete Funders </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/>  Delete Program </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> Delete Partner </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> Delete Quantity </a></li>
						<li><a href = "#"><img src = "images/arrow_button2.jpg"/> Delete FoodBank</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class = "right_col">
				<div id = "mysearchform">
	 			    	

				</div>	
				<!--
				<select name = "catid" id = "catid">
				   <option> Select a Category </option>
				-->
				   <?php
					/*
					$cats = GetCategories();
					while($cat = mysql_fetch_assoc($cats))
					     {
						echo "<option value ='".$cat['id']."'";
						echo ($record['catid'] == $cat['id'])? "Category Selected" :"";//inline conditional statement, checks if category already exist
						echo ">".$cat['catname']."</option>";
					     }
					*/
				   ?>
				<!--
				</select>
				<div id = "mysearchform">
		 			    	<form id = "searchbox" action "search.php" method = "get">
							 <input type = "text" size='20' id = "search" name="searchTerm" value='' placeholder="Search Contacts by Name" autofocus />
							 <input type = "submit" id = "submit" value = "Search"/>
						 </form>
					</div>
				</form> 
				-->
	
	

		</div>	
		</div>	
	<?php
?>
<body>
	

