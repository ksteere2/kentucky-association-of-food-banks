<?php
	// If the form was submitted, scrub the input (server-side validation)
	// see below in the html for the client-side validation using jQuery
	// collect all input and trim to remove leading and trailing whitespaces
	if($_POST) {
        $firstname = trim($_POST['first_name']);
	$lastname = trim($_POST['last_name']);
	$email = trim($_POST['email']);
	$zipcode = trim($_POST['zipcode']);
	$phone = trim($_POST['phone']);
	$address = trim($_POST['address']);
	$city = trim($_POST['city']);
	$county = trim($_POST['county']);	
  
 	 $errors = array();
  
	  // Validate the input
	  if (empty($firstname))
	    array_push($errors, "Please Enter Your First Name");
	  if (empty($lastname))
	    array_push($errors, "Please Enter Your Last Name");
	   if (empty($zipcode))
	    array_push($errors, "Please Enter Your Zipcode");
	   if (empty($phone))
	    array_push($errors, "Please Enter Your Phone Number");
	  if (empty($city))
	    array_push($errors, "Please Enter Your City");
	  if (empty($county))
	    array_push($errors, "Please Enter Your County");
	  if (empty($address)) 
	    array_push($errors, "Please Enter Your Address");
	  if (!filter_var($email, FILTER_VALIDATE_EMAIL))
	    array_push($errors, "Please Enter A Valid Email Address");
  
    
	    
	  // If no errors were found, proceed with storing the user input
	  if (!empty($errors)) {
	    array_push($errors, "No errors were found. Thanks!");
	  }
	  
	  //Prepare errors for output
	  $output = '';
	  foreach($errors as $val) {
	    $output .= "<div class='error_msg'>$val</div>";
	  }
	  
    }

?>
