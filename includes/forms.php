<?php
	include_once("includes/key_functions.php");
	/*Login Form at the home page */
	function DisplayLoginForm(){
		?>
		<h3 class = "Index_login"> Admin Login Portal </h3>
		<form method = "post" action "">
			<div for ="username"> Username</div><br/>
			<input type = "text" name ="username" id ="username" value = "" />
			<div class ="clear"></div>
			<div for="pass"> Password</div><br/>
			<input type = "password" name ="pass" id ="pass" value = "" />
			<div class ="clear"></div>
			<input type = "submit" name = "submitted" class = "greenBG" value = "Please Log Me In" />
			<div class ="clear"></div>
		</form>

		<?php
	  }
	/*Add a contact form*/
	function AddContactForm(){
		
		        ?>
						
			<h3 class = "greyBG"> Add New Contact</h3>
			<form action = "" method = "post" id="add-new-contact" name = "contactForm">
				<div for ="category" > Category </div>
				<select name = "catid" id = "catid">
				   <option> Select a category </option>
				   <?php
					$cats = GetCategories();
					while($cat = mysql_fetch_assoc($cats))
					     {
						echo "<option value ='".$cat['id']."'";
						echo ($record['catid'] == $cat['id'])? "Category Selected" :"";
						echo ">".$cat['catname']."</option>";
					     }
				   ?>
				</select>
				<div for ="first_name" > First Name </div>
				<input type = "text" name = "first_name" value = "" id = "first_name"/>
				<div for ="last_name" > Last Name </div>
				<input type = "text" name = "last_name" value = "" id = "last_name"/>
				<div for ="email" > Email </div>
				<input type = "text" name = "email" value = "" id = "email"/>
				<div for ="zipcode" > Zip Code </div>
				<input type = "text" name = "zipcode" value = "" id = "zipcode"/>
				<div for ="phone" > Phone No </div>
				<input type = "text" name = "phone" value = "" id = "phone"/>
				<div for ="city" > City </div>
				<input type = "text" name = "city" value = "" id = "city"/>
				<div for ="county" > County </div>
				<input type = "text" name = "county" value = "" id = "county"/>
				<div id = "comment"> Address </div>
				 <textarea id  = "comment"  name ="address" class = "mess2" ></textarea> 
				<br/><br/>
				<a href = "forms.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type = "submit" name = "add" id = "add" value = "Add New Contact" />
				<div class ="clear"></div>
			</form>

		<?php

		include_once("includes/addcontactvalidate.php");/*Validate Contact form not working yet*/
	}
	/*Update contact form */
	function UpdateContactForm($record){

		?>
			<h3 class = "greyBG"><?php if($record['id'] > 0){echo $record['first_name']; echo "\x20\x20\x20"; echo $record['last_name'];} ?></h3>
			<form action = "" method = "post" name = "contactForm">
				<div for ="category" > Category </div>
				<select name = "catid" id = "catid">
				   <option> Select a category </option>
				   <?php
					$cats = GetCategories();
					while($cat = mysql_fetch_assoc($cats))
					     {
						echo "<option value ='".$cat['id']."'";
						echo ($record['catid'] == $cat['id'])? "Category Selected" :"";//inline conditional statement, checks if category already exist
						echo ">".$cat['catname']."</option>";
					     }
				   ?>
				</select>
				<div for ="first_name" > First Name </div>
				<input type = "text" name = "first_name" value = "<?php echo $record['first_name'];?>" id = "first_name"/>
				<div for ="last_name" > Last Name </div>
				<input type = "text" name = "last_name" value = "<?php echo $record['last_name'];?>" id = "last_name"/>
				<div for ="email" > Email </div>
				<input type = "text" name = "email" value = "<?php echo $record['email'];?>" id = "email"/>
				<div for ="zipcode" > Zip Code </div>
				<input type = "text" name = "zipcode" value = "<?php echo $record['zipcode'];?>" id = "zipcode"/>
				<div for ="phone" > Phone No </div>
				<input type = "text" name = "phone" value = "<?php echo $record['phone'];?>" id = "phone"/>
				<div for ="city" > City </div>
				<input type = "text" name = "city" value = "<?php echo $record['city'];?>" id = "city"/>
				<div for ="county" > County </div>
				<input type = "text" name = "county" value = "<?php echo $record['county'];?>" id = "county"/>
				<div id = "comment"> Address </div>
				 <input type = "text"  name ="address" class = "mess2" value = "<?php echo $record['address'];?>" id  = "address" />
				<br/><br/>
				<a href = "displayallcontacts.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type="submit"   name = "update" id = "update" value="Update Contact" />
				<div class ="clear"></div>
				<input type = "hidden" value = "<?php echo $record['id']; ?>" name = "id" />
		
			
			</form>

		<?php


	}
	/* Add Category form */
	function AddCategoryForm()
	{
		?>
			<h3 class = "greyBG">Add New Category</h3>
			<form action = "" method = "post" name = "categoryForm">
				<div for = "catname" >Category Name</div>
				 <input type = "text" name = "catname" value = "" id = "commname"/>
				<br/><br/>
				<a href = "forms.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type = "submit" name = "add"  id = "add" value = "Add New Category" />
				<div class ="clear"></div>
			</form>

		<?php

	}
	/*Add Communication Type form*/
	function AddCommunicationTypeForm()
	{
		?>
			<h3 class = "greyBG">Add New Communication Type </h3>
			<form action = "" method = "post" name = "categoryForm">
				<div for = "catname" > Communication Type</div>
				 <input type = "text" name = "commtype" value = "" id = "catname"/>
				<br/><br/>
				<a href = "forms.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type = "submit" name = "add"  id = "add" value = "Add New Communication Type" />
				<div class ="clear"></div>
			</form>

		<?php

	}
	/*Update category form*/
	function UpdateCategoryForm($record)
	{
		?>
			
			<h3 class = "greyBG"><?php echo $record['catname'];?></h3>
			<form action = "" method = "post" name = "categoryForm">
				<div for = "catname" > Category Name </div>
				<input type = "text" name = "catname" value = "<?php echo $record['catname'];?>" id = "catname"/>
				<br/><br/>
				<a href = "displayallcontacts.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type="submit" name="update" id = "update" value="Update Category" />
				<div class ="clear"></div>
				<input type = "hidden" value = "<?php echo $record['id']; ?>" name = "id" />
			</form>

		<?php

	}
	/*Add Donation Form*/
	function AddDonationForm(){

		?>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> <!---The Javascript is for date picker -->
			<script src="//code.jquery.com/jquery-1.9.1.js"></script>
			<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
			<link rel="stylesheet" href="/resources/demos/style.css">
			<script>
				$(function() {
					$( "#datepicker" ).datepicker();
				});
			</script>			
			<h3 class = "greyBG"> Add New Donation </h3>
			<form action = "" method = "post" name = "contactForm">
				<div for ="category" > Category </div>
				<select name = "catid" id = "catid">
				   <option> Select a Category </option>
				   <?php
					$cats = GetCategories();
					while($cat = mysql_fetch_assoc($cats))
					     {
						echo "<option value ='".$cat['id']."'";
						echo ($record['catid'] == $cat['id'])? "Category Selected" :"";
						echo ">".$cat['catname']."</option>";
					     }
				   ?>
				</select>
				<div for ="contact" > Contact </div>
				<select name = "id" id = "id">
				<option> Select a Contact </option>
				<?php
				$cats = GetAllContacts();
				while($cat = mysql_fetch_assoc($cats))
				{		
					echo "<option value ='".$cat['id']."'";
					echo ($cat['catid'] == $cat['id'])? "Contact Selected" :"";
					echo ">".$cat['first_name']."</option>";
					echo "</div>";
				}
				 ?>	
				</select>
				<div for ="donation" > Donation Amount </div>
				<input type = "text" name = "amount" value = "" id = "donation"/>
				<div for ="donation" > Donation Date </div>
				<input type = "text" name = "date" value = "" id="datepicker" />
				<br/><br/>
				<a href = "forms.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type = "submit" name = "add"  id = "add" value = "Add New Donation" />
				<div class ="clear"></div>
		</form>

		<?php


	}
	/*Add Funders form */
	function AddFundersForm(){

		?>
			
			<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
			<script src="//code.jquery.com/jquery-1.9.1.js"></script>
			<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
			<link rel="stylesheet" href="/resources/demos/style.css">
			<script>
				$(function() {
					$( "#datepicker" ).datepicker();
				});
			</script>
			<h3 class = "greyBG"> Add New Funding </h3>
			<form action = "" method = "post" name = "contactForm">
				<div for ="category" > Category </div>
				<select name = "catid" id = "catid">
				   <option> Select a Category </option>
				   <?php
					$cats = GetCategories();
					while($cat = mysql_fetch_assoc($cats))
					     {
						echo "<option value ='".$cat['id']."'";
						echo ($record['catid'] == $cat['id'])? "Category Selected" :"";
						echo ">".$cat['catname']."</option>";
					     }
				   ?>
				</select>
				<div for ="contact" > Contact </div>
				<select name = "id" id = "id">
				<option> Select a Contact </option>
				<?php
				$cats = GetAllContacts();
				while($cat = mysql_fetch_assoc($cats))
				{		
					echo "<option value ='".$cat['id']."'";
					echo ($cat['catid'] == $cat['id'])? "Contact Selected" :"";
					echo ">".$cat['first_name']."</option>";
					echo "</div>";
				}
				 ?>	
				</select>
		
				<div for ="donation" > Funding Amount </div>
				<input type = "text" name = "funding_amount" value = "" id = "donation"/>
				<div for ="donation" > Funding Organization/Company </div>
				<input type = "text" name = "funding_org" value = "" id = "donation"/>
				<div for ="donation" > Funding Date </div>
				<input type = "text" name = "funding_date" value = "" id="datepicker"/>
				<br/><br/>
				<a href = "forms.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type = "submit" name = "add"  id = "add" value = "Add New Funding" />
				<div class ="clear"></div>
		</form>

		<?php


	}
	/*Track Communication Form */
	function DisplayInfoTrackForm(){
		
		?>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.9.1.js"></script>
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css">
		<script>
			$(function() {
				$( "#datepicker" ).datepicker();
			});
		</script>
		<h3 class = "greyBG"> New Communication </h3>
			<form action = "" method = "post" name = "TrackComForm">
				<div for ="category" > Category </div>
				<select name = "catid" id = "catid">
				   <option> Select a Category </option>
				   <?php
					$cats = GetCategories();
					while($cat = mysql_fetch_assoc($cats))
					     {
						echo "<option value ='".$cat['id']."'";
						echo ($record['catid'] == $cat['id'])? "Category Selected" :"";
						echo ">".$cat['catname']."</option>";
					     }
				   ?>
				</select>
				<div for ="contact" > Contact </div>
				<select name = "id" id = "id">
				<option> Select a Contact </option>
				<?php
				$cats = GetAllContacts();
				while($cat = mysql_fetch_assoc($cats))
				{		
					echo "<option value ='".$cat['id']."'";
					echo ($cat['catid'] == $cat['id'])? "Contact Selected" :"";
					echo ">".$cat['first_name']."</option>";
					echo "</div>";
				}
				 ?>	
				</select>
				<div for ="contact" > New Communication </div>
				<select name = "id" id = "id">
				<option> Select a Communication type </option>
				<?php
				$cats = GetAllContacts();
				while($cat = mysql_fetch_assoc($cats))
				{		
					echo "<option value ='".$cat['id']."'";
					echo ($cat['catid'] == $cat['id'])? "Contact Selected" :"";
					echo ">".$cat['first_name']."</option>";
					echo "</div>";
				}
				 ?>	
				</select>
				<br/><br/>
				<div id = "date-picker" > Date </div>
				<input type ="text" id="datepicker"/>
				<div id = "comment"> Comment </div>
				 <textarea id  = "comment"  name ="comment" class = "mess" ></textarea> 
				<br/><br/>
				<a href = "forms.php" name = "cancel"class = "cancel"> Cancel </a>
				<input type = "submit" name = "action"  id = "add" value = "Save New Communication" />
				<div class ="clear"></div>
				
			</form>

		<?php
		
	
	}
	/*Add New Food Bank Form*/
	function AddFoodBankForm(){

		?>
			<h3 class = "greyBG"> Add New Food Bank </h3>
			<form action = "" method = "post" name = "contactForm">
				<div for ="category" > Category </div>
				<select name = "catid" id = "catid">
				   <option> Select a category </option>
				   <?php
					$cats = GetCategories();
					while($cat = mysql_fetch_assoc($cats))
					     {
						echo "<option value ='".$cat['id']."'";
						echo ($record['catid'] == $cat['id'])? "Category Selected" :"";//inline conditional statement, checks if category already exist
						echo ">".$cat['catname']."</option>";
					     }
				   ?>
				</select>
				<div for ="fb_name" > Food Bank Name </div>
				<input type = "text" name = "fb_name" value = "" id = "fb_name"/>
				<div id = "comment"> Food Bank Address </div>
				 <textarea id  = "comment"  name ="fbank_address" class = "mess2" ></textarea>
				<br/><br/>
				<a href = "forms.php" name = "cancel" class = "cancel"> Cancel </a>
				<input type = "submit" name = "add"  id = "add" value = "Add New Food Bank" />
				<div class ="clear"></div>
			</form>

		<?php


	}

	/*search form */
	function Search_Form(){
			?>
				<div class = "admin_main_col">
	 				<form  id = "searchbox"action "<?=$PHP_SELF?>" method = "get">
						 <input type = "text" size='20' id = "search" name="searchTerm" value='' placeholder="Search Contacts by Name" autofocus />
						<input type = "submit" name = "submit" id = "submit" value = "Search Contacts"/>
					</form>
				</div>	

			<?php

	}

?>
