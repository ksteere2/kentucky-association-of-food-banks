<?php
	session_start();	
	include_once("includes/key_functions.php");/*Include Keyfunctions ---- this is where connection function and querries reside*/
	include_once("includes/forms.php");/*All the forms for the site is here*/
	include_once("includes/header.php");/*This is the header for the forms*/
	echo "<div class = 'formDivMain'>";/*This DIV is defined in the main.css file*/
	echo "<br/>";
	echo "<div class = 'formDiv'>";
	AddFoodBankForm();/*This function is created in the form.php file and is the add food bank form*/
	echo "</div>";	
	echo "</div>";
	if (isset($_POST['add'])){
	 	 $saveFBank = SaveFoodBank($_POST);/*Save FoodBank is a function that queries the database and does inserts*/
		if($saveFBank) 
		{		  
			header("location: admin.php");/*If contact saved correctly, redirect the user to the admin page*/
		}
		else
		{
		 DisplayErrorMessage("Oops, There was an error saving new category");	
		}
	     
	} 
?>
