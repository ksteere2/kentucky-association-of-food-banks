<?php
	session_start();	
	include_once("includes/key_functions.php");/*Include Keyfunctions ---- this is where connection function and querries reside*/
	include_once("includes/forms.php");/*All the forms for the site is here*/
	/*If form has not been submitted*/
	include_once("includes/header.php");/*This is the header for the forms*/
	echo "<div class = 'formDivMain'>";/*This DIV is defined in the main.css file*/
	echo "<br/>";
	echo "<div class = 'formDiv'>";
	AddContactForm();/*This function is created in the form.php file and is the add contact form*/
	echo "</div>";
	echo "</div>";
	
	if (isset($_POST['add'])){
		 
	  	 $savedCont = SaveContact($_POST);/*Save Contact is a function that queries the database and does inserts*/
		if($savedCont) 
		{						
			 DisplayStatusMessage("New Contact Added Successfully");
			 //sleep(4);			
			 header("location: displayallcontacts.php");/*If contact saved correctly, redirect the user to the admin page*/
		}
		else
		{
		 DisplayErrorMessage("Oops, there was an error saving your new contact");/*Display Error message, the function is defined in key_functions.php*/	
		}
	}
	
?>
