<?php
	include_once("includes/key_functions.php");/*Include Keyfunctions ---- this is where connection function and querries reside*/
	include_once("includes/forms.php");/*All the forms for the site is here*/
	/*If form has not been submitted*/
	include_once("includes/header.php");/*This is the header for the forms*/
	$id = intval($_GET['id']); /*Fetch the id of the current record in order to update it*/
	$contact_results = GetContact($id);/*The function to get contact is defined in key_functions.php*/
	$contact = mysql_fetch_assoc($contact_results);
	echo "<div class = 'formDivMain'>";
	echo "<br/>";
	echo "<div class = 'formDiv'>";
	UpdateContactForm($contact);/*This function is created in the form.php file and is used to update contact*/
	echo "</div>";
	echo "</div>";
	if (isset($_POST['update'])) {
	        $updated = UpdateContact($_REQUEST);/* The updatecontact function is defined in key_functions.php*/
		if($updated)
		{	  
		    header("location: displayallcontacts.php"); /*If contact updated correctly, redirect the user*/	
		}
		else
		{
		  DisplayErrorMessage("Oops, there was an error updating your contact");	
						    
		}
	} 
?>
