<?php
/*

Operations of this APP:
1. Login Users  -----------------------------------------------------------------------: Done
2. Logout Users -----------------------------------------------------------------------: Done
3. Add Categories to the Database -----------------------------------------------------: Done
4. Add Users' Contact Details to the Database (Each User must be added to a Category)--: Done
5. Update Categories in the Datatabase ------------------------------------------------: Done
6. Update Contacts in the Database ( Associated with Categories) ----------------------: Done
7. Delete A category and All associated Contacts --------------------------------------:
9. Delete a contact from a category ---------------------------------------------------:
10.Track Communications between Admin and Contacts ------------------------------------:
11.Create forms to handle new Entries for specific categories e.g donations etc -------: Ongoing
12.

*/
/*Start a session*/
session_start();
/*Check if user is in session*/
/*If user is not logged, then it means user is not in session*/
if(isset($_SESSION['LogIn']))
{
	header("location:admin.php");		
}
else
{
	include_once("includes/forms.php");/*All the forms on the application is defined here*/
	include_once("includes/key_functions.php");
	
	if(isset($_POST['submitted']))
	{
	     if( ProcessLogin($_POST) == true)/*This function is defined in kafb_functions.php.*/ 
	        {
			header("location:admin.php"); /*If login was successful, redirect user to admin page */
	        }
		else
		{
			
			include_once("includes/index_header.php");/*The header to the login page */			
			echo "<div class = 'loginform'>";
			DisplayLoginForm();/*Calls the Display login function in forms.php*/
			echo "</div>";
			DisplayErrorMessage("Oops, I can't Let You In! Check your Username/Password");/*If password or username is wrong, display this message*/
			include_once("includes/footer.php");/*This is the footer for the login page*/
			
	
    		}
   	}
       else
       {
		include_once("includes/index_header.php");/*The header to the login page */
		echo "<div class = 'loginform'>";/*The login form div is defined in main.css*/
		DisplayLoginForm();/*Calls the Display login function in forms.php*/
		echo "</div>";
		include_once("includes/footer.php");/*This is the footer for the login page*/
	
        }
	
}
?>
