<?php
	include_once("includes/key_functions.php");/*Include Keyfunctions ---- this is where connection function and querries reside*/
	include_once("includes/forms.php");/*All the forms for the site is here*/
	include_once("includes/header.php");/*This is the header for the forms*/
	$id = intval($_GET['id']); /*Fetch the id of the current record in order to update it*/
	$categories_results = GetCategory($id);
	$cat = mysql_fetch_assoc($categories_results);
	echo "<div class = 'formDivMain'>";
	echo "<br/>";
	echo "<div class = 'formDiv'>";
	UpdateCategoryForm($cat);/*This function is created in the form.php file and is used to update category*/
	echo "</div>";	
	echo "</div>";
	if (isset($_POST['update'])) {
	   	$updated = UpdateCategory($_REQUEST);/* The updatecategory function is defined in key_functions.php*/
		if($updated)
		{
		   header("location:displayallcontacts.php"); /*If category is updated correctly, redirect the user to the admin page*/	
		}
		else
		{
		  DisplayErrorMessage("Oops, There was an error updating category");	
						    
		}
	} 

?>
